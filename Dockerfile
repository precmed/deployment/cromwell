FROM openjdk:8 as ci-env

RUN apt-get update && \
    apt-get install -y \
            apt-utils \
            apt-transport-https \
            build-essential \
            ca-certificates \
            curl \
            gnupg \
            gnupg2 \
            python-dev \
            mysql-client \
            software-properties-common \
            sudo && \
    echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && \
    apt-get update && \
    apt-get install -y sbt

RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python get-pip.py && \
    pip install --upgrade --force-reinstall pyopenssl

FROM ci-env as build
WORKDIR /cromwell
COPY . /cromwell
RUN sbt assembly && \
    mv $(find /cromwell/server/target/ | grep 'cromwell-.*\.jar$') ./cromwell.jar

FROM openjdk:8-jre-stretch as prod

COPY --from=build /cromwell/cromwell.jar /cromwell/cromwell.jar
COPY --from=build /cromwell/docker/run.sh  /cromwell/run.sh
WORKDIR /cromwell
EXPOSE 8080
ENTRYPOINT [ "sh", "/cromwell/run.sh" ]

FROM prod as prod-with-cwltool
RUN apt-get update && \
    apt-get install -q -y python python-pip && \
    pip install cwltool
